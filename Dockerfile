FROM golang:1.15

WORKDIR /dwltd
COPY ./cmd cmd


RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]