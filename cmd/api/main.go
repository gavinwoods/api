package main

import (
	"fmt"

	"github.com/labstack/echo/v4"
	em "github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	db2 "gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/repo"
	"gitlab.com/InfiniteDaremo/api/internal/server"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"go.uber.org/zap"
)

const (
	IngressPort        = "PORT"
	DBConnectionString = "DATABASE_URL"
)

func init() {
	viper.SetDefault(IngressPort, 8080)
}

func main() {
	viper.AutomaticEnv()
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	swagger, err := spec.GetSwagger()
	if err != nil {
		logger.Fatal(fmt.Sprintf("Error loading swagger spec\n: %s", err))
	}

	swagger.Servers = nil

	cs := viper.GetString(DBConnectionString)
	ds, err := db2.NewDatabase(cs)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Error loading postgrest connection\n: %s", err))
	}
	r := repo.NewRepo()
	instance := server.NewServer(ds, r, logger)

	e := echo.New()
	e.Use(em.Logger())

	// Serve api
	server.RegisterHandlers(e, instance, r, ds)

	// TODO Embed file once 1.16 releases and heroku supports it
	// Serve redocly
	e.File("/docs", "index.html")

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt(IngressPort))))

}
