BUILDDIR=./bin

build: clean doc
	@ mkdir -p $(BUILDDIR)
	go build -o $(BUILDDIR)/api ./cmd/api/main.go
	redoc-cli -o $(BUILDDIR)/index.html bundle openapi/swagger.yaml

run:
	go run ./cmd/api/main.go

.PHONY: dep
dep:
	go mod download

# Run all tests.
.PHONY: test
test: dep
	go test -cover -race ./...

# Remove Go binaries.
.PHONY:
clean:
	rm -rvf $(BUILDDIR)

# Generate boilerplate
.PHONY: gen
gen:
	go generate ./...

# Generate redoc static html
.PHONY: doc
doc:
	redoc-cli -o index.html bundle openapi/swagger.yaml

.PHONY: migrate
migrate:
	migrate -database ${DB_URL} -path db/migrations up

.PHONY: seed
seed:
	psql -h localhost -U user -d dwltd -f ./db/seed/seed.sql

# Generate sqlboiler models
.PHONY: sqlboiler
sqlboiler:
	sqlboiler psql --struct-tag-casing=camel --no-hooks --wipe --no-tests
