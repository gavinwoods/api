# api

## Requirements

1. oapi-codegen - This will be retrieved by running make gen
2. migrate - ```brew install golang-migrate```
3. sqlboiler - https://github.com/volatiletech/sqlboiler (This does not need to be a project dependency)
4. heroku CLI - brew install heroku/brew/heroku (heroku login)

## Generate OpenAPI Spec
```bash
make gen
```

## Postgres
### Migrations
```
// Create a migration
migrate create -ext sql -dir db/migrations -seq migration_name

// Export db connection string
export DATABASE_URL='postgres://user:password@localhost:5432/dwltd?sslmode=disable'
migrate -database ${DATABASE_URL} -path db/migrations up

// If a migration fails rollback to the last non-dirty migration
migrate -database ${DATABASE_URL} -path db/migrations force 1
```

### SQL Boiler
```
make sqlboiler
```

### Seeding
```
make seed
```

For production seeding:
```
heroku pg:psql -f ./db/seed/seed.sql
```

### Deployment
```
heroku login
export DATABASE_URL=$(heroku config:get DATABASE_URL -a dwltd-api)
git push origin
```

If any issues occur run ```heroku logs --tail```