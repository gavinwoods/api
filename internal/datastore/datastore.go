package datastore

import (
	"database/sql"

	_ "github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/v4/boil"
)

type IDatastore interface {
	boil.ContextExecutor
}

type Datastore struct {
	*sql.DB
}

func NewDatabase(connStr string) (*Datastore, error) {
	db, err := sql.Open(
		"postgres",
		connStr)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &Datastore{
		db,
	}, nil
}

// Interface assertions
var _ IDatastore = (*Datastore)(nil)
