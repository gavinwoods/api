package middleware

import (
	"github.com/labstack/echo/v4"
	db2 "gitlab.com/InfiniteDaremo/api/internal/datastore"
	"gitlab.com/InfiniteDaremo/api/internal/repo"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"net/http"
)

func EmployeeMiddleware(repo *repo.Repo, store db2.IDatastore) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			name := c.Param(spec.EmployeeNameKey)
			e, err := repo.Employees.GetEmployee(c.Request().Context(), store, name)

			if err != nil {
				return c.JSON(http.StatusNotFound, err)
			}

			c.Set(spec.EmployeeNameKey, e)

			return next(c)
		}
	}
}