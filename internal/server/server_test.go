package server

import (
	"github.com/deepmap/oapi-codegen/pkg/middleware"
	"github.com/labstack/echo/v4"
	em "github.com/labstack/echo/v4/middleware"
	"gitlab.com/InfiniteDaremo/api/internal/repo"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"go.uber.org/zap"
	"os"
	"testing"
)

var e *echo.Echo

func TestMain(m *testing.M) {
	e = echo.New()
	defer e.Close()

	logger, _ := zap.NewProduction()
	defer logger.Sync()

	swagger, err := spec.GetSwagger()
	if err != nil {
		panic(err)
	}

	swagger.Servers = nil

	e.Use(middleware.OapiRequestValidator(swagger))
	e.Use(em.Logger())

	r := repo.NewMockRepo()

	instance := NewServer(nil, r, logger)

	RegisterHandlers(e, instance, r, nil)

	os.Exit(m.Run())
}
