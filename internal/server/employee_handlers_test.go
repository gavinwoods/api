package server

import (
	"github.com/deepmap/oapi-codegen/pkg/testutil"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func Test_GetEmployees(t *testing.T) {
	res := testutil.NewRequest().Get("/employees").Go(t, e)
	assert.Equal(t, http.StatusOK, res.Code())
}

func Test_GetEmployeeName(t *testing.T) {
	res := testutil.NewRequest().Get("/employees/gavin").Go(t, e)
	assert.Equal(t, http.StatusOK, res.Code())

	res = testutil.NewRequest().Get("/employees/missing").Go(t, e)
	assert.Equal(t, http.StatusNotFound, res.Code())
}
