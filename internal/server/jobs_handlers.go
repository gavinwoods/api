package server

import (
	"errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"gitlab.com/InfiniteDaremo/api/internal/view"
	"gitlab.com/InfiniteDaremo/api/models"
	"net/http"
)

func (s Server) GetEmployeesNameJobs(ctx echo.Context, _ spec.Name) error {
	ee := ctx.Get(spec.EmployeeNameKey).(*models.Employee)
	j, err := s.repo.Jobs.GetJobs(ctx.Request().Context(), s.ds, ee)

	if err != nil {
		var noContent noContentError
		switch {
		case errors.As(err, &noContent):
			return ctx.String(noContent.Code(), noContent.Error())
		default:
			return ctx.String(http.StatusInternalServerError, err.Error())
		}
	}

	return ctx.JSON(http.StatusOK, view.JobsView(j))
}
