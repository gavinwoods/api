package view

import (
	"gitlab.com/InfiniteDaremo/api/internal/spec"
	"gitlab.com/InfiniteDaremo/api/models"
)

func EmployeeView(e *models.Employee) *spec.Employee {

	return &spec.Employee{
		Dob:  spec.UnixDate(e.Dob.Unix()),
		Id:   e.ID,
		Name: e.Name,
	}
}

func JobsView(j []*models.Job) []*spec.Job {
	jobs := make([]*spec.Job, len(j))
	for i, job := range j {

		achievements := make([]spec.Achievement, len(job.R.Achievements))
		for i, a := range job.R.Achievements {
			achievements[i] = spec.Achievement(a.Description)
		}

		jobs[i] = &spec.Job{
			Description:  job.Description,
			EmployeeId:   job.EmployeeID,
			Id:           job.ID,
			Role:         job.Role,
			StartDate:    spec.UnixDate(job.StartDate.Unix()),
			Title:        job.Title,
			Achievements: achievements,
		}

		if job.EndDate.Valid {
			d := spec.UnixDate(job.EndDate.Time.Unix())
			jobs[i].EndDate = &d
		}
	}
	return jobs
}
